Name:           perl-Module-Install-ReadmeMarkdownFromPod
Version:        0.04
Release:        11
Summary:        Create README.mkdn from POD
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Module-Install-ReadmeMarkdownFromPod

Source0:        https://cpan.metacpan.org/authors/id/M/MA/MATTN/Module-Install-ReadmeMarkdownFromPod-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  findutils glibc-common make perl-interpreter >= 1:5.6.0 perl-generators perl(base) perl(Carp)
BuildRequires:  perl(CPAN) >= 1.89 perl(Cwd) perl(ExtUtils::Command) perl(ExtUtils::MakeMaker) perl(ExtUtils::Manifest)
BuildRequires:  perl(ExtUtils::MM_Unix) perl(File::Find) perl(File::Path) perl(File::Spec) perl(FindBin) perl(lib)
BuildRequires:  perl(Pod::Parser) perl(Pod::Text) perl(strict) perl(Test::Builder::Module) perl(Test::Harness)
BuildRequires:  perl(URI::Escape) perl(vars) perl(warnings)
Requires:       perl(Module::Install) perl(Module::Install::ReadmeFromPod) perl(Pod::Markdown)

%description
Module::Install::ReadmeMarkdownFromPod is a Module::Install extension that generates a "README.mkdn" 
file automatically from an indicated file containing POD whenever the author runs "Makefile.PL". This
file is used by GitHub to display nicely formatted information about a repository.

%package_help

%prep
%autosetup -n Module-Install-ReadmeMarkdownFromPod-%{version}

iconv -f iso-8859-1 -t utf8 < README > README.utf8
mv README.utf8 README

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test

%files
%doc README
%{perl_vendorlib}/*

%files help
%doc Changes
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.04-11
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.04-10
- Package init
